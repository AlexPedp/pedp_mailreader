package com.pedp.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class MailProperties {
	
	private static MailProperties instance = null;
	
	 public static MailProperties getInstance() {
	      if(instance == null) {
	         instance = new MailProperties();
	      }
	      return instance;
	   }

	public void create(){
	 
		Properties prop = new Properties();
		OutputStream output = null;

		try {

			output = new FileOutputStream("application.properties");
			 
			prop.setProperty("inbox_folder", "inbox");
			prop.setProperty("saved_folder", "salvate");
			prop.setProperty("host", "pop.copego.it");
			prop.setProperty("user", "ordini@copego.it");
			prop.setProperty("password", "Pegaso@221");
			prop.setProperty("file_extension", "csv|xls|xlsx|pdf|doc|txt");
			prop.setProperty("printer_name", "\\\\server04\\Kyocera Mita KM-2050 KX");
			prop.setProperty("save_mail", "true");
			prop.setProperty("stop_path", "C:\\Users\\alessandrom\\workspace\\Ordini-PDF\\import.sem");
			
			// save properties to project root folder
			prop.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public Map<String, String> load(){
		
		Map<String, String> map = new HashMap<String, String>();
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("application.properties");

			// load a properties file
			prop.load(input);
			
			for (final String name: prop.stringPropertyNames()) {
				map.put(name, prop.getProperty(name));
			}
			 

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return map;
	}
	
}
