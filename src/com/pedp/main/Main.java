package com.pedp.main;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.mail.MessagingException;

import com.pedp.logic.UnreadMail;
import com.pedp.properties.MailProperties;

public class Main {

	private static final int SLEEP = 10000;
	private static final int MAX_WAIT = 6;
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public static void main(String[] args) throws MessagingException, IOException {

		try {
			File f = new File("application.properties");
			if(!f.exists()) { 
				MailProperties.getInstance().create();
			}

			Map<String, String> map = MailProperties.getInstance().load();
			String stop_path = map.get("stop_path");
			File file = new File(stop_path);
			
			int counter = 0;
			while (counter < MAX_WAIT) {
				counter++;
				if(file.exists()) {
					System.out.println(sdf.format(new Date()) +  "|Presenza file semaforo: " + stop_path + "|sleep: " + SLEEP);
					Thread.sleep(SLEEP);
					continue;
				}
				UnreadMail um = new UnreadMail(map);
				um.fetchMessages();
				break;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		
		

	}
 

}
