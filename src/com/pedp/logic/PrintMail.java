package com.pedp.logic;

import java.awt.Desktop;
import java.awt.print.PrinterJob;
import java.io.File;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.printing.PDFPageable;
import org.apache.pdfbox.printing.PDFPrintable;
import org.apache.pdfbox.printing.Scaling;
import org.apache.pdfbox.text.PDFTextStripper;

public class PrintMail {

	private static final String CARREFOUR = "CARREFOUR";
	private static final String AUCHAN = "AUCHAN";
	private static final String MARGHERITA = "MARGHERITA";
	
	private final String TXT = "txt";
	private final String PDF = "pdf";
	private final String XLSX = "xlsx";
	private final String XLSM = "xlsm";
	private final String XLS = "xls";
	private final String DOC = "doc";
	private final String TIF = "tif";
	
	private File file;
	private String printer;
	
	public PrintMail(File file, String printer){
		this.file = file;
		this.printer = printer;
	}
	
	public void print() {
		
		getDefaultPrinter();
		
		String extension  = getFileExtension(file);

		switch (extension.toLowerCase()) {
		case TXT:
			System.out.println("Printing txt....");
			printOthers();
			break;
		case PDF:
			System.out.println("Printing pdf....");
			printPdf();
			break;
		case XLSX:
			System.out.println("Printing xlsx....");
			printOthers();
			break;
		case XLSM:
			System.out.println("Printing xlsm....");
			printOthers();
			break;
		case XLS:
			System.out.println("Printing xls....");
			printOthers();
			break;
		case DOC:
			System.out.println("Printing doc....");
			printOthers();
			break;
		case TIF:
			System.out.println("Printing tif....");
			printOthers();
			break;
		default:
			break;
		}
	}
		
	 
	
	public void printPdf(){

		try {
			PDDocument document = PDDocument.load(file);

			PDFPageable p = new PDFPageable(document);
			PDFPrintable printable = null;

			PrintService myPrintService = findPrintService(printer);
			PrinterJob job = PrinterJob.getPrinterJob();
			job.setPageable(new PDFPageable(document));
			job.setPrintService(myPrintService);
			
			
			PDPage page = document.getPage(0);

			PDRectangle mediaBox = page.getMediaBox();
			boolean isLandscape = mediaBox.getWidth() > mediaBox.getHeight();
			int rotation = page.getRotation();
			if (rotation == 90 || rotation == 270)
				isLandscape = !isLandscape;
			
			if (isLandscape && isPageToScale(document))  {
				printable = new PDFPrintable(document,Scaling.SCALE_TO_FIT);
				job.setPageable(p);
				job.setPrintable(printable);
			} 
//				else {
//				printable = new PDFPrintable(document,Scaling.ACTUAL_SIZE);
//			}

			
			job.print();
		} catch (Exception e) {
			e.printStackTrace();
		}


	}
	
	public void getDefaultPrinter(){
		PrintService service =  PrintServiceLookup.lookupDefaultPrintService();
			if (service != null) {
			    String printServiceName = service.getName();
			    System.out.println("Print Service Name is " + printServiceName);
			} else {
			    System.out.println("No default print service found");
			}
	}
	
	public void printOthers() {
		
		if(!Desktop.isDesktopSupported()){
		    System.out.println("Desktop is not supported");
		    return;
		}
 		
		try {
			Desktop.getDesktop().print(file);
		} catch (Exception e) {
		 
		}
		
	}

	private static PrintService findPrintService(String printerName) {
		PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);

		for (PrintService printService : printServices) {
			System.out.println(printService.getName());
			if (printService.getName().trim().equals(printerName)) {
				return printService;
			}
		}
		return null;
	}

	public static boolean isPageToScale(PDDocument document){
		boolean isPageToScale = false;
		try {
			String s =  new PDFTextStripper().getText(document);
			if (s.contains(CARREFOUR) || (s.contains(AUCHAN) || (s.contains(MARGHERITA)))){
				isPageToScale = true;
				return isPageToScale;
			}
		} catch (Exception e) {
			isPageToScale = false;
			return isPageToScale;
		}
		return isPageToScale;
	}
	
	private String getFileExtension(File file) {
	    String name = file.getName();
	    try {
	        return name.substring(name.lastIndexOf(".") + 1);
	    } catch (Exception e) {
	        return "";
	    }
	}

}
