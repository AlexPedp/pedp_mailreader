package com.pedp.logic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.FlagTerm;

import org.apache.commons.io.FilenameUtils;

public class UnreadMail {

	Map<String, String> map = new HashMap<String, String>();
	Pattern regex = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+", Pattern.CASE_INSENSITIVE);

	public UnreadMail(Map<String, String> map){
		this.map = map;
	}


	public void fetchMessages() throws MessagingException, IOException {

		String host = map.get("host");
		String user = map.get("user");
		String password = map.get("password");
		String inbox = map.get("inbox_folder");
		String saved = map.get("saved_folder");
		String[] extensions = map.get("file_extension").toString().split("\\|");
		String printer = map.get("printer_name");
		boolean mailToSave = Boolean.parseBoolean(map.get("save_mail"));
		boolean read = false;

		Properties properties = new Properties();
		properties.put("mail.store.protocol", "imaps");
		properties.put("mail.imaps.partialfetch", "false");
		System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		System.setProperty("mail.mime.decodeparameters", "true");

		Session emailSession = Session.getDefaultInstance(properties);
		Store store = emailSession.getStore();
		store.connect(host, user, password);


		Folder emailFolder = store.getFolder(inbox);
		emailFolder.open(Folder.READ_WRITE);

		Folder defaultFolder = store.getDefaultFolder(); 
		Folder savedFolder = store.getFolder(saved);
		if (!savedFolder.exists()){
			createFolder(defaultFolder, saved);	
		}


		Flags seen = new Flags(Flags.Flag.SEEN);
		FlagTerm unseenFlagTerm = new FlagTerm(seen, read);


		Message[] messagges = emailFolder.search(unseenFlagTerm);
		String subject = null;
		String sender = null;

		for (int i=0; i < messagges.length;i++)    {

			System.out.println("*****************************************************************************");
			System.out.println("MESSAGE " + (i + 1) + ":");
			Message message =  messagges[i];


			subject = message.getSubject();
			sender = message.getFrom()[0].toString();

			Matcher matcher = regex.matcher(sender);
			while (matcher.find()) {
				sender = matcher.group();
				break;
			}

			System.out.println("Subject: " + subject);
			System.out.println("Date: " + message.getSentDate());
			System.out.println("From: " + message.getFrom()[0]);
			System.out.println("Content-Type:" + message.getContentType());

			if (message.getContent() instanceof Multipart) {
				Multipart multipart = (Multipart) message.getContent();

				String contentType = multipart.getContentType();
				
				for (int x = 0; x < multipart.getCount(); x++) {
					Part part = multipart.getBodyPart(x);
					String attachmentFileName =  part.getFileName();
					System.out.println("File-name:" + attachmentFileName);
					
					String disposition = part.getDisposition();

					if (disposition == null && attachmentFileName !=null){
						
						String extension = FilenameUtils.getExtension(attachmentFileName).toLowerCase();

						String saveDir = System.getProperty("user.dir") + File.separator + sender.replace("@", "_").replace(" ", "").replace(".", "_");

						File directory = new File(saveDir);
						if (! directory.exists()){
							directory.mkdir();
						}

						File fileToSave = new File(saveDir + File.separator + UUID.randomUUID() + "_" + attachmentFileName);
						System.out.println("File to save: " + fileToSave.getAbsolutePath());
						
						boolean contains = Arrays.asList(extensions).contains(extension);
						if (!contains) {
							continue;
						}
						
						InputStream is = part.getInputStream();
						FileOutputStream fos = new FileOutputStream(fileToSave);
						byte[] buf = new byte[4096];
						int bytesRead;
						while((bytesRead = is.read(buf))!=-1) {
							fos.write(buf, 0, bytesRead);
						}
						fos.close();
					}


					if ((disposition != null) && ((disposition.equalsIgnoreCase(Part.ATTACHMENT) || (disposition.equalsIgnoreCase(Part.INLINE))))) {

						try {

							MimeBodyPart mimeBodyPart = 	mimeBodyPart = (MimeBodyPart) part;
							String fullPath = mimeBodyPart.getFileName();
							String fileName = FilenameUtils.getName(fullPath);
							String extension = FilenameUtils.getExtension(fullPath).toLowerCase();


							boolean contains = Arrays.asList(extensions).contains(extension);
							if (!contains) {
								continue;
							}


							String saveDir = System.getProperty("user.dir") + File.separator + sender.replace("@", "_").replace(" ", "").replace(".", "_");

							File directory = new File(saveDir);
							if (! directory.exists()){
								directory.mkdir();
							}

							File fileToSave = new File(saveDir + File.separator + UUID.randomUUID() + "_" + fileName);
							mimeBodyPart.saveFile(fileToSave);
							if (printer.trim().length()>0){
								PrintMail pm = new PrintMail(fileToSave, printer);
								pm.print();	
							}

						} catch (Exception e) {
							System.out.println("Problem in saving file");
						}

					}
				}
			}

			try {
				emailFolder.setFlags(new Message[] {message}, new Flags(Flags.Flag.SEEN), true);
				if (mailToSave) {
					emailFolder.copyMessages(new Message[] {message}, savedFolder);	
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		emailFolder.close(false);
		store.close();

	}

	private boolean createFolder(Folder parent, String folderName)  {   
		boolean isCreated = true;   

		try  
		{   
			Folder newFolder = parent.getFolder(folderName);   
			isCreated = newFolder.create(Folder.HOLDS_MESSAGES);   
			System.out.println("created: " + isCreated);   

		} catch (Exception e)   
		{   
			System.out.println("Error creating folder: " + e.getMessage());   
			e.printStackTrace();   
			isCreated = false;   
		}   
		return isCreated;   
	}
 

}
